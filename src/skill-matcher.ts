export type Candidate = {
  name: string;
  skills: string[];
};

export class SkillMatcher {
  private candidates: Candidate[];

  constructor(candidates: Candidate[]) {
    this.candidates = candidates;
  }

  /**
   * Renvoie les candidats ayant les compétences demandées
   */
  public getCandidatesHavingSkills(skills: string[]): Candidate[] {
    return this.candidates.filter((candidate) =>
      skills.every((skill) => candidate.skills.includes(skill))
    );
  }

  /**
   * Renvoie un objet indiquant la "fréquence" des compétences dans la base de candidats.
   *
   * Par exemple : { HTML: 7, JavaScript: 4, Git: 9 }
   */
  public getSkillStats(): Record<string, number> {
    const allSkills = this.candidates.flatMap((candidate) => candidate.skills);
    return allSkills.reduce((stats, skill) => {
      stats[skill] = (stats[skill] || 0) + 1;
      return stats;
    }, {} as Record<string, number>);
  }
}
