export const add = (a: number, b: number): number => a + b;

export const sub = (a: number, b: number): number => a - b;

export const mul = (a: number, b: number): number => a * b;

export const div = (a: number, b: number): number => {
  if (b === 0) {
    throw new Error("Can't divide by zero");
  }
  return a / b;
};

export const divAsync = async (a: number, b: number): Promise<number> => {
  if (b === 0) {
    return Promise.reject(new Error("Can't divide by zero"));
  }
  return Promise.resolve(a / b);
};
