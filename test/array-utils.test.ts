import {
  removeSecondFromArr,
  findFirstEven,
  keepEven,
} from "../src/array-utils";

it("should remove the second element", () => {
  const actual = removeSecondFromArr([1, 2, 3, 5, 7]);
  expect(actual).toEqual([1, 3, 5, 7]);
});

describe("findFirstEven", () => {
  it("should return something", () => {
    const actual = findFirstEven([5, 9, 14, 17]);
    expect(actual).toBeDefined();
  });
  it("should return nothing", () => {
    const actual = findFirstEven([5, 9, 13, 17]);
    expect(actual).toBeUndefined();
  });
});

describe("filterEven", () => {
  it("result should not contain 1", () => {
    const actual = keepEven([1, 2, 4, 6]);
    expect(actual).not.toContain(1);
  });
  it("result should contain 6", () => {
    const actual = keepEven([3, 6, 9]);
    expect(actual).toContain(6);
  });
});
