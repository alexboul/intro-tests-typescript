export const removeSecondFromArr = (arr: any[]) =>
  arr.filter((it: any, i: number) => i !== 1);

export const findFirstEven = (arr: number[]) =>
  arr.find((n: number) => n % 2 === 0);

export const keepEven = (arr: number[]) =>
  arr.filter((n: number) => n % 2 === 0);
