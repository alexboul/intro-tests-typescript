import { SkillMatcher } from "../src/skill-matcher";

describe("skill-matcher", () => {
  describe("getCandidatesHavingSkills", () => {
    it("should return candidates with the required skills", () => {
      const candidates = [
        { name: "Alice", skills: ["HTML", "CSS"] },
        { name: "Bob", skills: ["HTML", "JavaScript"] },
        { name: "Charlie", skills: ["Git", "JavaScript"] },
      ];
      const skillMatcher = new SkillMatcher(candidates);
      const actual = skillMatcher.getCandidatesHavingSkills(["HTML"]);
      expect(actual).toEqual([
        { name: "Alice", skills: ["HTML", "CSS"] },
        { name: "Bob", skills: ["HTML", "JavaScript"] },
      ]);
    });
  });

  describe("getSkillStats", () => {
    it("should return the frequency of each skill", () => {
      const candidates = [
        { name: "Alice", skills: ["HTML", "CSS"] },
        { name: "Bob", skills: ["HTML", "JavaScript"] },
        { name: "Charlie", skills: ["Git", "JavaScript"] },
      ];
      const skillMatcher = new SkillMatcher(candidates);
      const actual = skillMatcher.getSkillStats();
      expect(actual).toEqual({ HTML: 2, CSS: 1, JavaScript: 2, Git: 1 });
    });
  });
});
