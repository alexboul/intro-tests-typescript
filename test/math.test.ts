import { add, sub, mul, div, divAsync } from "../src/math";

function throwsError() {
  throw new Error("Error");
}

describe("math", () => {
  describe("add", () => {
    it("should return 5 for 2 + 3", () => {
      const expected = 5;
      const actual = add(2, 3);
      expect(actual).toBe(expected);
    });

    it("should return 9 for 4 + 5", () => {
      const expected = 9;
      const actual = add(4, 5);
      expect(actual).toBe(expected);
    });
  });

  describe("sub", () => {
    it("should return 5 for 15 - 10", () => {
      const expected = 5;
      const actual = sub(15, 10);
      expect(actual).toBe(expected);
    });

    it("should return 3 for 8 - 5", () => {
      const expected = 3;
      const actual = sub(8, 5);
      expect(actual).toBe(expected);
    });
  });

  describe("mul", () => {
    it("should return 6 for 2 * 3", () => {
      const expected = 6;
      const actual = mul(2, 3);
      expect(actual).toBe(expected);
    });

    it("should return 20 for 4 * 5", () => {
      const expected = 20;
      const actual = mul(4, 5);
      expect(actual).toBe(expected);
    });
  });

  describe("div", () => {
    it("should return 5 for 15 / 3", () => {
      const expected = 5;
      const actual = div(15, 3);
      expect(actual).toBe(expected);
    });

    it("should throw an error for 15 / 0", () => {
      expect(() => {
        div(15, 0);
      }).toThrow("Can't divide by zero");
    });
  });
  describe("divAsync function", () => {
    it("should resolve a number", async () => {
      // ici on "await" pas la promise
      const actual = divAsync(24, 4);
      // mais on await le expect
      await expect(actual).resolves.toBe(6);
    });

    it("should reject", async () => {
      // ici on "await" pas la promise
      const actual = divAsync(7, 0);
      // mais on await le expect
      await expect(actual).rejects.toThrow("Can't divide by zero");
    });
  });
});
